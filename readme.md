# Robot operating system setup ansible playbook

This is a simple Ansible playbook for installing [ROS](http://www.ros.org/)
Kinetic on Ubuntu 16.04. It follows the instructions [specified
here](http://wiki.ros.org/kinetic/Installation/Ubuntu).

The Ansible playbook is `ros.yaml`, and acts on the `ros` hosts.